# OER Würfel

> Wer diesen Würfel in seiner Hand hält, der ist in weniger als einer Stunde in der Lage, freie Bildungsmaterialien zu finden und einen ersten Kurs, eine Projektarbeit oder einfach die Vorbereitung auf eine Übung rechtssicher und auffindbar im Netz bereitzustellen.   

... so das Ziel dieses Projekts ;-)

## Vorlage für den Bau eines Faltwürfels mit OER infos.

Wer mag, kann den Würfel ausdrucken und selbst basteln (s. Youtube: Faltwürfel basteln). Die Grafiken sind aber auch darauf ausgelegt, mit finanziellem Einsatz einen Würfel über einen Online-Dienst professionell bedrucken zu lassen. Einfach mal nach "Faltwürfel" googeln.

Im Ordner IMAGES liegen die Originale der Würfel-Seiten als SVG vor (mit Inkscape erstellt). Die entsprechenden PNG Grafiken können alternativ für den Ausdruck verwendet werden, wenn z.B. die Schriften in den SVG nicht richtig angezeigt werden.

Das Projekt selbst wird als CC-BY bereitgestellt, wegen der Abhängigkeit zur Domain oer-wuerfel.de. Die Bilder werden als CC0 lizensiert und können beliebig genutzt und verändert werden. Ausnahme sind die folgenden aufgebrachten Grafiken:

* OER-Logo UNESCO : https://www.unesco.de/bildung/open-educational-resources
* SDG4-Logo UN : https://www.un.org/sustainabledevelopment/education
* ggf. Bilder zu Lizenzen, Tools und Workflows

(TODO: Lizenztext und Verweis einfügen!)

![Würfel dunkel](images/wuerfel-dunkel.jpg)

![Würfel hell](images/wuerfel-hell.jpg)



## Oben
![Oberseite](images/wuerfel-oben.png)

## Vorne
Wenn du Schüler/Student bist, und alles lernen können möchtest, was dich interessiert, dann mach OER, und hilf anderen mit deinen Lernergebnissen (Hausübungen, Übungsvorbereitungen etc.) erfolgreich zu lernen.

Wenn du Lehrer/Dozent bist, dein Fach liebst, und möchtest, dass so viele Menschen wie möglich das gleiche fühlen wie du, dann mach OER, und hilf anderen mit deinen Lehrmaterialien (Erläuterungen, Aufgaben mit Lösungen etc.) deine Begeisterung mit ihnen zu teilen.

![Vorderseite](images/wuerfel-vorne.png)

## Rechts
![Rechteseite](images/wuerfel-rechts.png)

## Hinten
![Rückseite](images/wuerfel-hinten.png)

## Links
![Linkeseite](images/wuerfel-links.png)

## Unten

* Ansprechpartner -> Verweis auf Seite mit freiwilligen Einträgen

![Unterseite](images/wuerfel-unten.png)

## Innen 1 - lang

* Die Grafiken im Innenbereich müssen anders angeordnet werden, so dass beim hellen Würfel alle Seiten vollständig sind, sie aber auch im aufgeklappten Zustand nicht zu wirr aussehen.
* Die Übergänge im aufgeklappten Zustand bei den dunklen Seiten sollten auch daraufhin noch einmal überprüft werden.
* Links auf Subdomains münzen?

![Innenseite1](images/wuerfel-innen1.png)

// Produkte streichen, die kommen auf die Webseite. Der Würfel selbst bleibt Produktfrei!

## Innen 2 - lang
![Innenseite2](images/wuerfel-innen2.png)

## Innen 3
![Innenseite3](images/wuerfel-innen3.png)

## Innen 4
![Innenseite4](images/wuerfel-innen4.png)

## TODO

* Webseite gnerieren, z.B. mit Hugo
* Impressum und Datenschutz
* Detailseiten erstellen und verlinken
